from sqlmodel import SQLModel, Field, text, Column, TIMESTAMP, create_engine, UniqueConstraint, Session as SQLModelSession
from typing import Optional
import os
from datetime import datetime
from datetime import date


class User(SQLModel, table=True):
    __table_args__ = (UniqueConstraint("user_name", name="unique_username"),)
    id: Optional[int] = Field(primary_key=True)
    permission_level: int = 0
    user_name: str = Field(min_length=2)
    password: str = Field(min_length=10)
    name: Optional[str] = Field(min_length=2)


# Create models in DB
def create_db_and_tables():
    SQLModel.metadata.create_all(engine)


# Function to get DB session
def get_session() -> SQLModelSession:
    with SQLModelSession(engine) as session:
        yield session


def get_db_session():
    return SQLModelSession(engine)


engine = create_engine(os.environ.get('DB_CONNECTION') or 'sqlite:///database.db', echo=True)
create_db_and_tables()