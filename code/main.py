from fastapi import Depends, FastAPI, HTTPException, status, UploadFile
from models import *
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
import jwt, json, os, time, datetime, base64, logging, requests, urllib.parse, re
from passlib.hash import bcrypt
from sqlmodel import Session, select, create_engine
from typing import Annotated
from pymongo import MongoClient


#logger
log_level = os.environ.get('LOG_LEVEL') or 'INFO'
logger = logging.getLogger("pp_logger")
logger.setLevel(log_level)
handler = logging.StreamHandler()
handler.setLevel(log_level)
formatter = logging.Formatter("[%(asctime)s] [%(levelname)s] [%(funcName)s:%(lineno)d] %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)


JWT_SECRET = os.environ.get('JWT_SECRET') or 'supersupersecret'
app = FastAPI()
# auth configuration
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")
engine = create_engine(os.environ.get('DB_CONNECTION') or 'sqlite:///database.db', echo=True)

logger.info("app started")

# Funzione per autenticare l'utente
def authenticate_user(username: str, password: str) -> User:
    session = Session(engine)
    query = select(User).where(User.username == username)
    user = session.exec(query).first()
    session.close()
    if not user or user.password != password:
        return None
    return user

# get application info
@app.get("/")
def test():
    """
        Perform test and check it's all ok,
        this test will also provide server timestamp
    """
    return {
        "info": "this app uses UTC time",
        "unix": datetime.datetime.utcnow().timestamp(), 
        "datetime": datetime.datetime.utcnow()
    }


# heartbeat for applicaton
@app.get("/hb")
def heartbeat():
    logger.info("called hb")
    return {"status": "ok"}

# API to create new user
if os.environ.get('NEW_USER_ENABLED') or False:
    @app.post("/create_user/")
    def generate_new_user(user: User):
        try:
            session = Session(engine)
            logger.info("creating new user")
            hashed_password = bcrypt.hash(user.password)
            user.password = hashed_password
            session.add(user)
            session.commit()
            session.refresh(user)
            session.close()
            logger.info("user created")
            return {"result":"ok"}
        except Exception as ex:
            logger.error(ex)
            raise HTTPException(status_code=400, detail=str(ex))


@app.post('/token/')
def generate_token(form_data: OAuth2PasswordRequestForm = Depends()):
    """
    Get token by providing username and password, token at the moment has no expiration
    """
    logger.info("generating token")
    user = authenticate_user(form_data.username, form_data.password)

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, 
            detail='Invalid username or password'
        )    

    token = jwt.encode({"id":user.id, "username": user.user_name, "password":user.password}, JWT_SECRET)
    logger.info("token created")
    return {'access_token' : token, 'token_type' : 'bearer'}




"""
Functions no API
"""
# access by token
def get_current_user(token):
    try:
        session = Session(engine)
        payload = jwt.decode(token, JWT_SECRET, algorithms=['HS256'])
        query = select(User).where(User.id == payload.get('id'))
        user = session.exec(query).first()
        session.close()
        return user
    except Exception as ex:
        logger.error(ex)
        return None


# authenticate user
def authenticate_user(username: str, password: str) -> User:
    try:
        session = Session(engine)
        query = select(User).where(User.user_name == username)
        user = session.exec(query).first()
        session.close()
        if not user or not bcrypt.verify(password, user.password):
            return None
        return user
    except Exception as ex:
        logger.error(ex)
        return None
